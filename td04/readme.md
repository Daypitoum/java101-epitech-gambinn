# TD4 : JSTL/Language EL/Validation des formulaires/Introduction JPA?

## Objectifs du TD

### Rappels
+ Récap du TD00, TD0, TD01, TD02, TD03
+ MVC

### Techniques
+ repartir du td04/td04-handson (version annotée) 
+ JSTL
+ Language EL
+ Validation avec JSR 303


####EL LANGUAGE####

>This shows a simple example of Unified EL being used within a JSTL "c:out" tag:
>
><c:out value="${myBean.myField}" />
>
>An expression that calls a method with a parameter:
>
>${myBean.addNewOrder('orderName')}


####JSTL####

> The JavaServer Pages Standard Tag Library (JSTL), is a component of the Java EE Web application development platform. It extends the JSP specification by adding a tag library of JSP tags for common tasks, such as XML data processing, conditional execution, database access, loops and internationalization.

> http://docs.oracle.com/javaee/5/jstl/1.1/docs/tlddocs/fn/tld-summary.html

> http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/view.html


####Validation####

> Validation is a JSR 303 : Reference implementation: Hibernate Bean Validator
> http://hibernate.org/validator/documentation/getting-started/
> http://docs.jboss.org/hibernate/stable/validator/reference/en-US/html




## TD04 ##


### Rappels des besoin

* 1 cours = 1 prof + 1 horaire
* un cours est fait pendant une plage horaire (toute les semaines) (eg. JAVA tous les lundi de 15h à 18h)
* deux cours ne peuvent pas se chevaucher
* lors de la création d'un nouveau cours, système doit proposer 3 horaires disponibles sur la semaine dans la tranche des Lundi-Vendredi ; 9h-12h30 14h-18h. Si aucun des 3 horaires ne convient, le système doit afficher 3 autres horaires et ainsi de suite.

* Pages à créer: 
* /cours qui affiche la liste des cours avec leurs horaires et les prof associés
* /profs qui affiche la liste des prof avec leurs cours associés. Il affiche l'emploi du temps d'un prof
* /nouveau-cours qui affiche un formulaire qui permet noter le nom du cours, le prof associé (dans la liste des profs), et qui propose de choisir l'horaire du cours dans une liste.







