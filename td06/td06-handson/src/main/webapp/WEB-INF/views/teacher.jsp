<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url value="/" var="webappRoot"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${webappRoot}/resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="${webappRoot}/resources/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste des cours</title>
</head>
<body>
	<div class="hero-unit">
		<h1>
			<c:out value="${teacher.name}"></c:out>
		</h1>
		<p>
			<c:out value="${teacher.mail}"></c:out>
		</p>
	</div>

	<table class="table">
		<tr>
			<th>Nom</th>
			<th>Dur�e</th>
		</tr>
		<c:forEach var="cours" items="${courses}">
			<tr>

				<td><c:out value="${cours.name}"></c:out></td>

				<td><c:out value="${cours.duration}"></c:out></td>
			</tr>
		</c:forEach>
	</table>

	

</body>
</html>