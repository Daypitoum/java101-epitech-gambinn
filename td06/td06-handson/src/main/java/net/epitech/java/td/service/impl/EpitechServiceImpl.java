package net.epitech.java.td.service.impl;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mysema.query.Query;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.QCourse;
import net.epitech.java.td.domain.QTeacher;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;
import net.epitech.java.td.repositories.CourseRepository;
import net.epitech.java.td.repositories.TeacherRepositoy;
import net.epitech.java.td.repositories.custom.TeacherRepositoryCustom;
import net.epitech.java.td.service.EpitechService;

@Service
public class EpitechServiceImpl implements EpitechService {

	@Inject
	CourseRepository courseRepo;

	@Inject
	TeacherRepositoy teacherRepo;
	
	@Inject
	TeacherRepositoryCustom custom;
	
	@Inject
	EntityManagerFactory emFactory;

	public Collection<Course> getCourses() {
		return courseRepo.findAll();

	}

	public Collection<Teacher> getTeachers() {
		return teacherRepo.findAll();

	}

	public void deleteCourse(Integer id) throws Exception {
		// TODO Auto-generated method stub

	}

	public Teacher addTeacher(String name, String mail) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteTeacher(Integer id) throws Exception {
		// TODO Auto-generated method stub

	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EpitechServiceImpl.class);

	@Override
	public Course addCourse(String name, String dayInWeek, Integer duration)
			throws FailedToParseDayInWeekException {
		
		LOGGER.trace("Je suis rentré  dans la fonction addCourse");
		Course course = new Course();
		if (dayInWeek.toLowerCase().equals("lundi")) {
			course.setDayInWeek(1);
		} else {
			LOGGER.warn("Mauvais jour de la semaine %s",dayInWeek);
			throw new FailedToParseDayInWeekException();
		}

		Teacher t = teacherRepo.findByName("Nicolas");
		course.setDuration(duration);
		course.setTeacher(t);
		course.setName(name);

		courseRepo.saveAndFlush(course);
		LOGGER.info("Un nouveau cours a été crée %s",course.getName());
		return course;

	}


	@Override
	public Collection<Teacher> getTeacherWithNoCourses() {
		return custom.getUnemployedTeachers();

	}

}
