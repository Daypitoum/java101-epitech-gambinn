package net.epitech.java.td01.td0ioc.config;

import net.epitech.java.td01.td0ioc.model.Teacher;
import net.epitech.java.td01.td0ioc.model.contract.Nameable;
import net.epitech.java.td01.td0ioc.model.contract.Teachable;
import net.epitech.java.td01.td0ioc.model.inj.EnglishCourse;
import net.epitech.java.td01.td0ioc.model.types.SexType;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class FirstYearCourseModuleConfiguration extends AbstractModule {

	@Override
	protected void configure() {
		bind(Nameable.class).annotatedWith(Names.named("english")).toInstance(
				new Teacher("Charlotte", SexType.FEMALE));
		bind(Nameable.class).annotatedWith(Names.named("java")).toInstance(
				new Teacher("Nicolas", SexType.MALE));
		

	}

}
